#include <systemc.h>
#include "reg_file.hpp"

using namespace std;
using namespace sc_core;

void reg_file::write()
{
   while(true)
   {
      wait();
      if (reg_write->read())
         locations[(write_reg->read()).to_uint()]= write_data->read();
   }
}

void reg_file::read()
{
   while(true)
   {
      wait();
      read_data1.write(locations[(read_reg1->read()).to_uint()]);
      read_data2.write(locations[(read_reg2->read()).to_uint()]);
   }
}
