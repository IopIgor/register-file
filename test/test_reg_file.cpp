#include <systemc.h>
#include <iostream>
#include "reg_file.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(5, SC_NS);
const sc_time clk_semiperiod(1, SC_NS);
const unsigned bit_data = 32;
const unsigned bit_addr = 5;
const unsigned mem_util = 10;

SC_MODULE(TestBench) 
{
 public:
  sc_signal<bool> clk;
  sc_signal<sc_bv<bit_addr> > read_reg1;
  sc_signal<sc_bv<bit_addr> > read_reg2;
  sc_signal<sc_bv<bit_addr> > write_reg;
  sc_signal<sc_bv<bit_data> > write_data;
  sc_signal<bool> reg_write;

  sc_signal<sc_bv<bit_data> > read_data1;
  sc_signal<sc_bv<bit_data> > read_data2;

  reg_file memo;

  SC_CTOR(TestBench) : memo("memo")
  {
    SC_THREAD(clk_thread);
    SC_THREAD(stimulus_and_watcher_thread);
      sensitive << clk;
    SC_THREAD(check)
      sensitive << read_reg1 << read_reg2;

    memo.clk(this->clk);
    memo.read_reg1(this->read_reg1);
    memo.read_reg2(this->read_reg2);
    memo.write_reg(this->write_reg);
    memo.write_data(this->write_data);
    memo.reg_write(this->reg_write);
    
	  memo.read_data1(this->read_data1);
    memo.read_data2(this->read_data2);
  }

  bool Check() const {return error;}
 
 private:
  bool error;
  bool done;

  void clk_thread()
  {
    done = 0;
    clk.write(0);
    while(!done)
    {
      wait(clk_semiperiod);
      if(clk.read() == 0) clk.write(1);
      else clk.write(0);
    }
  }

  void stimulus_and_watcher_thread() 
  {    
    //write some data's in some registers
    reg_write.write(1);
    unsigned i = 0;
    while(i < mem_util)
    {
      wait();
      if(clk.read() == 1)
      {
        i++;
        write_reg.write(i);
        write_data.write(i);
        cout << "At time " << sc_time_stamp() << ": writed " << write_data.read() 
             << " in address " << write_reg.read().to_uint() << endl;
      }
    }

    reg_write.write(false);
    //read data from first output
    for(unsigned j = 1; j < mem_util; j++)
    {
      read_reg1.write(j);
      wait(wait_time);
    }

    //read data from second output
    for(unsigned k = 1; k < mem_util; k++)
    {
      read_reg2.write(k);
      wait(wait_time);
    }

    done = 1;
  }

  void check() 
  {
    error = 0;

    while(!done)
    {
      wait();
      wait(1, SC_PS); //to check before memory update
      if(((read_reg1.read()).to_uint() != (read_data1.read()).to_uint()) || 
          ((read_reg2.read()).to_uint() != (read_data2.read()).to_uint()))
      {
        cout << "test failed!!\nfirst input-first output: " << (read_reg1.read()).to_uint() 
             << " - " << (read_data1.read()).to_uint() << "\nsecond input-second output:"
             << (read_reg2.read()).to_uint() << " - " << (read_data2.read()).to_uint() << endl 
             << endl;
        error = 1;
      }
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.Check();
}

