#ifndef REG_FILE_HPP
#define REG_FILE_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(reg_file) 
{
  static const unsigned bit_addr = 5;
  static const unsigned bit_data = 32;
  static const unsigned MEM_SIZE = 32;

  sc_in<bool> clk;
  sc_in<sc_bv<bit_addr> > read_reg1;
  sc_in<sc_bv<bit_addr> > read_reg2;
  sc_in<sc_bv<bit_addr> > write_reg;
  sc_in<sc_bv<bit_data> > write_data;
  sc_in<bool> reg_write;

  sc_out<sc_bv<bit_data> > read_data1;
  sc_out<sc_bv<bit_data> > read_data2;
  
  SC_CTOR(reg_file) 
  {
    SC_THREAD(write);
      sensitive << clk.pos();

    SC_THREAD(read)
      sensitive << read_reg1 << read_reg2 << clk;

    for (unsigned i=0; i<MEM_SIZE; i++) //memory intialized to 0
      locations[i]=0;
  } 

  sc_bv<bit_data> Loc(unsigned i) const {return locations[i];}
  
 private:
  sc_bv<bit_data> locations[MEM_SIZE];

  void write();
  void read();
};

#endif
